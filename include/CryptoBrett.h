#ifndef CRYPTOBRETT_H
#define CRYPTOBRETT_H

#include "Crypto.h"


class CryptoBrett : public Crypto
{
    public:
        CryptoBrett();
        virtual ~CryptoBrett();
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
        std::string lettersLower;
        std::string lettersCaps;
        int i;
        int k;
};

#endif // CRYPTOBRETT_H
