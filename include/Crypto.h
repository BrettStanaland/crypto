#ifndef CRYPTO_H
#define CRYPTO_H

#include <string>

/**
 *  The Crypto class is an interface.  The "encode" and "decode" functions are
 *  pure virtual functions and render the Crypto class unable to be instantiated.
 */
class Crypto {
    public:
        Crypto();
        virtual ~Crypto();

        // These are pure virtual functions and make it so the class is unable to be instantiated.

        /**
         * encodes a string of text
         *
         * @param s - the input string
         * @return the encoded string
         */
        virtual std::string encode(std::string s) = 0;

        /**
         * decodes a string of text
         *
         * @param s - the input string
         * @return the decoded string
         */
        virtual std::string decode(std::string s) = 0;
    protected:
    private:
};

#endif // CRYPTO_H
