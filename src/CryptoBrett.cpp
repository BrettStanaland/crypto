#include "../include/CryptoBrett.h"
#include <string>
#include <iostream>

using namespace std;

CryptoBrett::CryptoBrett()
{
    lettersLower = "abcdefghijklmnopqrstuvwxyz";
    lettersCaps ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    i = 0;
    k = 0;
}

CryptoBrett::~CryptoBrett()
{
    //dtor
}

string CryptoBrett::encode(string s) {

    while (s[i]){
        while(k < 26){
                if (s[i] == lettersLower[k]){
                        if (k >= 13){
                            s[i] = lettersLower[k -13];
                            k = 26;
                        }
                        if (k <= 12){
                            s[i] = lettersLower[k + 13];
                            k = 26;
                        }
                }
                if (s[i] == lettersCaps[k]){
                        if (k >= 13){
                            s[i] = lettersCaps[k -13];
                            k = 26;
                        }
                        if (k <= 12){
                            s[i] = lettersCaps[k + 13];
                            k = 26;
                        }
                }
            k++;
        }
        i++;
        k=0;
    }
    return s;
}


string CryptoBrett::decode(string s) {
    i = 0;
    while (s[i]){
        while(k < 26){
                if (s[i] == lettersLower[k]){
                        if (k >= 13){
                            s[i] = lettersLower[k -13];
                            k = 26;
                        }
                        if (k <= 12){
                            s[i] = lettersLower[k + 13];
                            k = 26;
                        }
                }
                if (s[i] == lettersCaps[k]){
                        if (k >= 13){
                            s[i] = lettersCaps[k -13];
                            k = 26;
                        }
                        if (k <= 12){
                            s[i] = lettersCaps[k + 13];
                            k = 26;
                        }
                }
            k++;
        }
        i++;
        k=0;
    }
    return s;
}
