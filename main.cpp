#include <iostream>
#include <memory>

#include "include/Crypto.h"
#include "include/CryptoBrett.h"
#include "include/CryptoChain.h"

using namespace std;

int main() {

    CryptoChain cc;

    auto_ptr<CryptoBrett> c1(new CryptoBrett);
    auto_ptr<CryptoBrett> c2(new CryptoBrett);
    auto_ptr<CryptoBrett> c3(new CryptoBrett);


    cc.add(c1.get());
    cc.add(c2.get());
    cc.add(c3.get());
    try{
    string input;
    cout << "Please enter what you want encoded: ";
    getline(cin, input);
    int i = 0;
    while(i < input.length()){
        if (isdigit(input[i])){
            throw input;
        }
        i++;
    }
    string encoded = cc.encode(input);
    string decoded = cc.decode(encoded);

    cout << endl <<  input << " -> " << encoded << " -> " << decoded;
    }
    catch(...){
        cout << "That wasn't a proper input. No numbers!";
    }
}
